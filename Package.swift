// swift-tools-version:5.3
import PackageDescription

let package = Package(
    name: "Identomat",
    defaultLocalization: "en",
    platforms: [.iOS(.v10)],
    products: [
        .library(
            name: "identomat",
            targets: ["identomatSPM"]),
    ],
    dependencies: [
        .package(url: "https://github.com/stasel/WebRTC.git", from: "106.0.0")
    ],
    targets: [
        .binaryTarget(
            name: "identomat",
            path: "Frameworks/identomat.xcframework"
        ),
        .target(
            name: "identomatSPM",
            dependencies: ["identomat", "WebRTC"]
        )
    ]
)
